#!/usr/bin/env bash

ansible-playbook create_file.yml \
                 -i inventory/local/hosts \
                 -c local \
                 "$@"

